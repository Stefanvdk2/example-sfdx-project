# Example SFDX Project

## Button with Github template
[![Deploy](https://deploy-to-sfdx.com/dist/assets/images/DeployToSFDX.svg)](https://deploy-to-sfdc.herokuapp.com/?template=https://github.com/Stefanvdk/test-deploy-to-sfdx)

## Button with Gitlab template
[![Deploy](https://deploy-to-sfdx.com/dist/assets/images/DeployToSFDX.svg)](https://deploy-to-sfdc.herokuapp.com/?template=https://gitlab.com/Stefanvdk2/example-sfdx-project)

## Description of Files and Directories
The goal of this project is to show the 'Deploy to SFDX'-button
